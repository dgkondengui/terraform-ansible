# Permet  d'afficher les IP de mes instances en sortie 
# 1. Creer un fichier output dans le module
# 2. Creer un fichier output a la racine du projet

output "instance_ip" {
    value = module.ec2.ec2_public_ip
}

output "instance_tags" {
    value = module.ec2.ec2_tags
}



