provider "aws" {
  region                  = "us-east-1"
}

/*
# terraform {
#   backend "s3" {
#     bucket                  = "terraform-backend-gerold"
#     key                     = "gerold-dev.tfstate"
#     region                  = "us-east-1"
#     shared_credentials_file = "/home/gerold/.aws/credentials"
#   }
# }
*/

#Appel du module de création du sg
module "sg" {
  source        = "../modules/sg"
  author = "kouakam"

}

/*
#Appel module de création du volume
module "ebs" {
  source        = "../modules/ebs"
  dd_size = 5
  author = "kouakam"
}
*/



# Appel du module de création de l'adresse ip pulique

/*
module "eip" {
  source        = "../modules/eip"
}
*/


# Appel du module de création de ec2
module "ec2" {
  source        = "../modules/ec2"
  instance_count = 2
  author        = "kouakam"
  instance_type = "t2.micro"
  sg_name= "${module.sg.out_sg_name}"
  //public_ip = "${module.eip.out_eip_ip}"
  //public_ip = var.public_ip
  user = "ec2-user"
}


#//////////////////////////////////////////////////
#Creation des associations nécessaires entre nos ressources

/*
resource "aws_eip_association" "eip_assoc" {
  instance_id = module.ec2.out_instance_id
  allocation_id = module.eip.out_eip_id
}


resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = module.ebs.out_vol_id
  instance_id = module.ec2.out_instance_id
}
*/


