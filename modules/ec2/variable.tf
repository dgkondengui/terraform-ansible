variable "author" {
  type    = string
  default = "kouakam"
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "ssh_key" {
  type    = string
  default = "EC2Tutorial"
}

variable "sg_name" {
  type    = string
  default = "NULL"
}

variable "public_ip" {
  type    = string
  default = "NULL"
}

variable "amazon_account_number" {
 # default = "099720109477"
  default = "137112412989"
}

variable "az" {
  type    = string
  default = "us-east-1c"
}

variable "user" {
  type    = string
  default = "NULL"
}

variable "instance_count" {
    type = number
    default = "1"   
}