data "aws_ami" "app_ami" {
  most_recent = true
  owners = ["${var.amazon_account_number}"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "aws_instance" "kouakam-ec2" {
  ami             = data.aws_ami.app_ami.id
  instance_type   = var.instance_type
  count = var.instance_count
  key_name        = var.ssh_key
  availability_zone = "${var.az}"
  security_groups = ["${var.sg_name}"]
  tags = {
    Name = "serverNginx-${count.index + 1}"
  }

  root_block_device {
    delete_on_termination = true
  }

/*
  provisioner "local-exec" {
    command = " echo PUBLIC IP: ${var.public_ip} , ID: ${aws_instance.kouakam-ec2.id} , AZ: ${aws_instance.kouakam-ec2.availability_zone} >> infos_ec2.txt"
  }
  */
/*
  provisioner "remote-exec" {
    inline = [
      "sudo apt update -y",
      "sudo apt install -y nginx",
      "sudo systemctl start nginx",
      "sudo systemctl enable nginx"
    ] 
    connection {
      type        = "ssh"
      user        = "${var.user}"
      private_key = file("/home/gerold/${var.ssh_key}.pem")
      host        = "${self.public_ip}"
    }
  }
  */



}

