
output "ec2_public_ip" {
  value = aws_instance.kouakam-ec2[*].public_ip
}

output "ec2_tags" {
  value = aws_instance.kouakam-ec2[*].tags_all
}

output "out_instance_az" {
  value = aws_instance.kouakam-ec2[*].availability_zone
}